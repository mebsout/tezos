# SPDX-FileCopyrightText: 2023 TriliTech <contact@trili.tech>
# SPDX-FileCopyrightText: 2023 Nomadic Labs <contact@nomadic-labs.com>
#
# SPDX-License-Identifier: MIT

[package]
name = "evm-execution"
version = "0.1.0"
edition = "2021"

[lib]
crate-type = ["cdylib", "rlib"]

[dependencies]
thiserror = "1.0"
hex = "0.4"
num-bigint = "0.3"
num-traits = "0.2.8"
rlp = "0.5.2"

tezos_data_encoding = "0.4"
tezos_crypto_rs = { version = "0.4", default-features = false }

# Adding these to 'dev_dependencies' causes the rand feature in crypto to be enabled
# on wasm builds, when building the entire workspace.
rand = { version = "0.8", optional = true }
proptest = { version = "1.0", optional = true }
evm = { version = "0.35.0", default-features = false }
primitive-types = { version = "0.11.1", default-features = false }
sha2 = { version = "0.10.6", default-features = false }
sha3 = { version = "0.10.6", default-features = false }
ripemd = { version = "0.1.3", default-features = false }
libsecp256k1 = { version = "0.7", default-features = false, features = ["static-context", "hmac"] }
const-decoder = { version = "0.3.0" }

[dependencies.tezos_smart_rollup_core]
path = "../../kernel_sdk/tezos_smart_rollup_core"

[dependencies.tezos_smart_rollup_host]
path = "../../kernel_sdk/tezos_smart_rollup_host"

[dependencies.tezos_smart_rollup_debug]
path = "../../kernel_sdk/tezos_smart_rollup_debug"

[dependencies.tezos_smart_rollup_storage]
path = "../../kernel_sdk/tezos_smart_rollup_storage"

[dependencies.tezos_smart_rollup_encoding]
path = "../../kernel_sdk/tezos_smart_rollup_encoding"

[dependencies.tezos_smart_rollup_mock]
path = "../../kernel_sdk/tezos_smart_rollup_mock"

[features]
default = ["evm_execution"]
testing = ["rand", "proptest"]
evm_execution = []
